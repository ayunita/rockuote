import tokens from "../data/tokens.json";
import { getTokenAllowance, approveToken } from "./erc20";
import { toBigNumber } from "./decimal";
import { limitAllowance } from "../constant/number";

// This function swaps tokens if the source token is approved and within allowance
// It returns tx object if success, else false
export const swap = async (
  web3,
  account,
  dexInstance,
  sourceName,
  targetName,
  sourceAmount,
  targetAmount,
  setLoading
) => {
  try {
    const tokenAddress = tokens[sourceName].kovan;

    let approval = false;
    // Check for allowance
    let allowance = await getTokenAllowance(web3, account, tokenAddress);

    if (allowance <= 0) {
      setLoading({ isLoading: true, text: "Waiting for approval..." });
      approval = await approveToken(
        web3,
        account,
        tokenAddress,
        toBigNumber(limitAllowance)
      );
      setLoading({ isLoading: true, text: "Preparing for swapping..." });
    } else {
      approval = true;
    }

    if (approval) {
      setLoading({ isLoading: true, text: "Ready to swap..." });
      let result = await dexInstance.methods
        .swap(
          tokenAddress,
          tokens[targetName].kovan,
          toBigNumber(sourceAmount),
          toBigNumber(targetAmount)
        )
        .send({ from: account });
      if (result) {
        if (result.status) {
          return result;
        }
      }
    }
    return false;
  } catch (e) {
    console.log("Swap error: ", e);
    return false;
  }
};

// This function deposit token1 & token1 if both tokens are approved and within allowance
// It returns tx result of token1 & token2 if success, else false
export const deposit = async (
  web3,
  account,
  dexInstance,
  tokenName1,
  tokenName2,
  tokenAmount1,
  tokenAmount2,
  setLoading
) => {
  try {
    const tokenAddress1 = tokens[tokenName1].kovan;
    const tokenAddress2 = tokens[tokenName2].kovan;

    let approval = false;
    // Check for allowance for token 1
    let allowance1 = await getTokenAllowance(web3, account, tokenAddress1);
    if (allowance1 <= 0) {
      setLoading({ isLoading: true, text: "Waiting for approval..." });
      approval = await approveToken(
        web3,
        account,
        tokenAddress1,
        toBigNumber(limitAllowance)
      );
    }
    // Check for allowance for token 2
    let allowance2 = await getTokenAllowance(web3, account, tokenAddress2);
    if (allowance2 <= 0) {
      setLoading({ isLoading: true, text: "Waiting for approval..." });
      approval = await approveToken(
        web3,
        account,
        tokenAddress2,
        toBigNumber(limitAllowance)
      );
    }

    if (allowance1 > 0 && allowance2 > 0) {
      approval = true;
    }

    if (approval) {
      setLoading({ isLoading: true, text: "Ready to deposit Asset 1..." });
      let result1 = await dexInstance.methods
        .deposit(
          tokens[tokenName1].kovan,
          tokens[tokenName2].kovan,
          toBigNumber(tokenAmount2)
        )
        .send({ from: account });
      setLoading({ isLoading: true, text: "Ready to deposit Asset 2..." });
      let result2 = await dexInstance.methods
        .deposit(
          tokens[tokenName2].kovan,
          tokens[tokenName1].kovan,
          toBigNumber(tokenAmount1)
        )
        .send({ from: account });
      if (result1 && result2) {
        return { result1: result1, result2: result2 };
      }
    }
    return { result1: false, result2: false };
  } catch (e) {
    console.log("Deposit error: ", e);
    return { result1: false, result2: false };
  }
};

// This function returns the token pairs and the amount of each pooled tokens
// Example: { "bat/link": { amount1: 10, amount2: 1 } }
export const getAllPairsBalance = async (dexInstance) => {
  let pairs = {};

  // TODO: pass dynamic network name
  let tokenList = Object.keys(tokens).map((key) => ({
    key: key,
    value: tokens[key].kovan,
  }));
  try {
    for (let i = 0; i < tokenList.length; i++) {
      for (let j = 0; j < tokenList.length; j++) {
        const source = tokenList[i];
        const target = tokenList[j];
        if (source.key !== target.key) {
          let result = await dexInstance.methods.getPairsBalance(
            source.value,
            target.value
          ).call();
          
          // Check if the pair already exists
          const key = `${source.key}/${target.key}`;
          //console.log(key, result)
          const key_reverse = `${target.key}/${source.key}`;
          if (!pairs[key] && !pairs[key_reverse]) {
            pairs[key] = { amount1: 0, amount2: result }
          } else if (pairs[key_reverse]) {
            pairs[key_reverse] = { ...pairs[key_reverse] , amount1: result }
          }
        }
      }
    }

    return pairs;
  } catch (e) {
    console.log("getPairsBalance error:", e);
    return pairs;
  }
};
