// SPDX-License-Identifier: MIT
pragma solidity 0.8.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/security/ReentrancyGuard.sol";
import {SafeMath} from "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/math/SafeMath.sol";

/**
 * @notice This contract provides swap between pairs of tokens.
 */
contract Dex is ReentrancyGuard {

    // Token pairs amount balance
    // [token1 address][token2 address] = balance
    mapping(address => mapping(address => uint256)) private _pairsBalance;
    
    event Deposited(address indexed source, address indexed target, uint256 value);
    event Swapped(address indexed source, address indexed target, uint256 amount, uint256 price);
    event LiquidityUpdated(address indexed source, address indexed target, uint256 value);

    /**
     * @notice Deposits amount of target token to smart contract
     * @param source source token's contract address
     * @param target target token's contract address
     * @param amount amount of target token
     */
    function deposit(address source, address target, uint amount) external {
        require(amount > 0, "Dex: deposit amount needed");
        // Update liquidity
        _addLiquidity(source, target, amount);
        
         // Check token allowance
        uint256 allowance = ERC20(target).allowance(msg.sender, address(this));
        require(allowance >= amount, "Dex: check the token allowance");
        
        // Transfer token to Dex
        require(ERC20(target).transferFrom(msg.sender, address(this), amount), "Dex: transferFrom failed");
        
        emit Deposited(source, target, amount);
    }
    
    /**
     * @notice Deposits source token to smart contract and send target token to message sender
     * @param source source token's contract address
     * @param target target token's contract address
     * @param amount amount of (source) token will be deposited
     * @param price amount of (target) token will be sent
     */    
    function swap(address source, address target, uint256 amount, uint256 price) external nonReentrant() {
        require(amount > 0, "Dex: swap amount needed");
        require(price > 0, "Dex: deposit price needed");
 
        // Check deposited token allowance
        uint256 allowance = ERC20(source).allowance(msg.sender, address(this));
        require(allowance >= amount, "Dex: check the token allowance");
        
        // Check if Dex has enough balance
        uint256 dexBalance = ERC20(target).balanceOf(address(this));
        require(price <= dexBalance, "Dex: not enough tokens in the reserve");
        
        // Check if pair has liquidity
        require(_pairsBalance[source][target] > 0 && _pairsBalance[target][source] > 0, "Dex: not enough liquidity pair");
        
        // Deposit source token
        require(ERC20(source).transferFrom(msg.sender, address(this), amount), "Dex: swap transferFrom failed");
        _addLiquidity(target, source, amount);
        
        // Send target token
        require(ERC20(target).transfer(msg.sender, price), "Dex: swap transfer failed");
        _removeLiquidity(source, target, price);
        
        emit Swapped(source, target, amount, price);
    }

    /**
     * @notice Updates liquidity by adding the current pair balance by amount
     * @param source source token's contract address
     * @param target target token's contract address
     * @param amount amount of target token
     */
    function _addLiquidity(address source, address target, uint256 amount) private {
        uint256 newBalance = SafeMath.add(getPairsBalance(source, target), amount);
        _pairsBalance[source][target] = newBalance;
        emit LiquidityUpdated(source, target, amount);
    }
    
    /**
     * @notice Updates liquidity by subtracting the current pair balance by amount
     * @param source source token's contract address
     * @param target target token's contract address
     * @param amount amount of target token
     */
    function _removeLiquidity(address source, address target, uint256 amount) private {
        uint256 newBalance = SafeMath.sub(getPairsBalance(source, target), amount);
        _pairsBalance[source][target] = newBalance;
        emit LiquidityUpdated(source, target, amount);
    }

    /**
     * @notice Gets pairs balance
     * @param source source token's contract address
     * @param target target token's contract address
     */
    function getPairsBalance(address source, address target) public view returns(uint256 balance) {
        return _pairsBalance[source][target];
    }

}