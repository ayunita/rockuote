import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import Web3Provider from "./context/Web3Context";
import Landing from "./components/Layout/Landing";
import Navigator from "./components/Navigator/Navigator";
import Swap from "./components/Swap/Swap";
import Pool from "./components/Pool/Pool";
import AddLiquidity from "./components/Pool/AddLiquidity";

import "./App.scss";
import heroImage from "./images/Saly-1.svg";
import logo from "./images/logo-title.svg";

const App = () => {
  return (
    <Web3Provider>
      <Landing>
        <div className="logo absolute left top">
          <img src={logo} alt="logo" />
        </div>
        <div className="row hero">
          <div className="flex-1">
            <img className="hero__image" src={heroImage} alt="to the moon" />
          </div>
          <div className="flex-1 column">
            <NotificationContainer />
            <Router>
              <Navigator />
              <Switch>
                <Route exact path="/">
                  <Swap />
                </Route>
                <Route exact path="/swap">
                  <Swap />
                </Route>
                <Route exact path="/pool">
                  <Pool />
                </Route>
                <Route exact path="/add-liquidity">
                  <AddLiquidity />
                </Route>
              </Switch>
            </Router>
          </div>
        </div>
        <svg
          className="absolute bottom"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 1440 320"
        >
          <defs>
            <linearGradient id="gradient">
              <stop offset="5%" className="purple" />
              <stop offset="50%" className="blue" />
              <stop offset="95%" className="green" />
            </linearGradient>
          </defs>
          <path
            className="purple"
            fillOpacity="1"
            d="M0,128L30,154.7C60,181,120,235,180,234.7C240,235,300,181,360,144C420,107,480,85,540,85.3C600,85,660,107,720,138.7C780,171,840,213,900,224C960,235,1020,213,1080,181.3C1140,149,1200,107,1260,96C1320,85,1380,107,1410,117.3L1440,128L1440,320L1410,320C1380,320,1320,320,1260,320C1200,320,1140,320,1080,320C1020,320,960,320,900,320C840,320,780,320,720,320C660,320,600,320,540,320C480,320,420,320,360,320C300,320,240,320,180,320C120,320,60,320,30,320L0,320Z"
          ></path>
        </svg>
        <div className="footer absolute bottom left">
          &copy; 2021 DEX Capstone Project
        </div>
      </Landing>
    </Web3Provider>
  );
};

export default App;
