import React, { useState } from "react";
import Quote from "./Quote";
import ConfirmSwap from "./ConfirmSwap";

const Swap = () => {
  const [requestQuote, setRequestQuote] = useState(false);
  const [quote, setQuote] = useState({});

  return (
    <>
      {!requestQuote ? (
        <Quote requestQuote={setRequestQuote} setQuote={setQuote} />
      ) : (
        <ConfirmSwap requestQuote={setRequestQuote} quote={quote} />
      )}
    </>
  );
};

export default Swap;
