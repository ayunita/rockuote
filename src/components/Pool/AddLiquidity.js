import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import {
  Segment,
  Dimmer,
  Loader,
  Form,
  Input,
  Dropdown,
  Button,
  Icon,
} from "semantic-ui-react";
import { NotificationManager } from "react-notifications";
import { useHistory } from "react-router-dom";
import Page from "../Layout/Page";
import tokens from "../../data/tokens.json";
import { Web3Context } from "../../context/Web3Context";
import { getLatestPriceResponse, getPrice } from "../../helpers/linkPrice";
import { getTokenBalance } from "../../helpers/wallet";
import { toNumber } from "../../helpers/decimal";
import { defaultPairs } from "../../constant/default";
import { deposit } from "../../helpers/dex";

const AddLiquidity = () => {
  const history = useHistory();
  const [input, setInput] = useState(""); // "from" or "to" input
  const [from, setFrom] = useState({ ...defaultPairs.from }); // "from" input state
  const [to, setTo] = useState({ ...defaultPairs.to }); // "to" input state
  const [loading, setLoading] = useState(false); // show loading for button
  const [processing, setProcessing] = useState({ isLoading: false, text: "" }); // show loading for page

  const web3State = useContext(Web3Context);
  // If "from" symbol or web3 context is updated, then get account's token balance
  useEffect(() => {
    const updateBalance = async () => {
      // TODO: reset "from" balance if network id changed
      if (web3State.web3 && from.name) {
        let balance = await getTokenBalance(
          web3State.web3,
          web3State.account,
          tokens[from.name].kovan
        );
        if (balance) {
          balance = toNumber(balance);
          setFrom({ ...from, balance: balance });
        }
      }
    };
    updateBalance();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [web3State.web3, web3State.account, web3State.networkId, from.symbol]);

  // If "to" symbol or web3 context is updated, then get account's token balance
  useEffect(() => {
    const updateBalance = async () => {
      // TODO: reset "to" balance if network id changed
      if (web3State.web3 && to.name) {
        let balance = await getTokenBalance(
          web3State.web3,
          web3State.account,
          tokens[to.name].kovan
        );
        if (balance) {
          balance = toNumber(balance);
          setTo({ ...to, balance: balance });
        }
      }
    };
    updateBalance();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [web3State.web3, web3State.account, web3State.networkId, to.symbol]);

  // This function will update output state based on input state changes
  const handleTokenInput = (
    amount,
    nameFrom,
    nameTo,
    func,
    currentOutputState
  ) => {
    if (amount) {
      if (nameFrom !== nameTo) {
        // Call API if amount is defined
        const response = getLatestPriceResponse(
          tokens[nameFrom].to_usd.contract,
          tokens[nameTo].to_usd.contract
        );
        response.then((data) => {
          if (data) {
            const result = getPrice(amount, data);

            func({
              ...currentOutputState,
              amount: toNumber(result.price),
              rates: toNumber(result.rates),
            });
          }
          setLoading(false);
        });
      } else {
        // Handle if "from" token = "to" token
        func({
          ...currentOutputState,
          amount: amount,
          rates: 1,
        });
        setLoading(false);
      }
    } else {
      // Reset state if user does not enter data
      func({
        ...currentOutputState,
        amount: "",
        rates: "",
      });
      setLoading(false);
    }
  };

  // Handle changes on "from" input
  useEffect(() => {
    if (input === "from") {
      setLoading(true);
      const timeOutId = setTimeout(
        () => handleTokenInput(from.amount, from.name, to.name, setTo, to),
        500
      );
      return () => clearTimeout(timeOutId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [from.symbol, from.amount, from.rates, from.name]);

  // Handle changes on "to" input
  useEffect(() => {
    if (input === "to") {
      setLoading(true);
      const timeOutId = setTimeout(
        () => handleTokenInput(to.amount, to.name, from.name, setFrom, from),
        500
      );
      return () => clearTimeout(timeOutId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [to.symbol, to.amount, to.rates, to.name]);

  // This function will deposit tokens
  const handleSubmit = async () => {
    setProcessing({ isLoading: true, text: "Processing..." });

    deposit(
      web3State.web3,
      web3State.account,
      web3State.dex,
      from.name,
      to.name,
      from.amount,
      to.amount,
      setProcessing
    ).then((result) => {
      if (result.result1 && result.result2) {
        NotificationManager.success(
          `You added ${from.amount} ${tokens[from.name].text} and ${to.amount} ${tokens[to.name].text}`,
          "Success!",
          3000
        );
        history.push("/pool");
      } else {
        NotificationManager.error("Transaction failed :(", "Oops", 3000);
        history.push("/pool");
      }
    });
  };

  return (
    <Page title="Add Liquidity">
      <Segment style={{ height: "100vh" }}>
        <Dimmer active={processing.isLoading}>
          <Loader inverted content={processing.text} />
        </Dimmer>
        <Form onSubmit={handleSubmit}>
          <Form.Field>
            <div className="input-label">
              <label>Asset 1:</label>
            </div>
            <Input
              type="number"
              min="0"
              step="any"
              className="input-number"
              label={
                <Dropdown text={from.symbol}>
                  <Dropdown.Menu>
                    {Object.keys(tokens).map((t) => (
                      <Dropdown.Item
                        key={t}
                        content={
                          <DropDownContent
                            text={tokens[t].text}
                            desc={tokens[t].desc}
                            image={tokens[t].image}
                          />
                        }
                        text={tokens[t].text}
                        value={tokens[t].value}
                        onClick={(_, data) => {
                          setInput("from");
                          setFrom({
                            ...from,
                            name: data.value,
                            symbol: data.text,
                            rates: 1,
                          });
                        }}
                      />
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              }
              labelPosition="right"
              placeholder="0.00"
              value={from.amount}
              onChange={(e) => {
                setInput("from");
                setFrom({
                  ...from,
                  amount: e.target.value,
                  rates: 1,
                });
              }}
            />
            <BalanceLabel amount={from.balance} token={from.symbol} />
          </Form.Field>
          <Form.Field>
            <Icon name="plus" size="large" color="yellow" />
          </Form.Field>
          <Form.Field>
            <div className="input-label">
              <label>Asset 2:</label>
            </div>
            <Input
              type="number"
              min="0"
              step="any"
              className="input-number"
              label={
                <Dropdown text={to.symbol}>
                  <Dropdown.Menu>
                    {Object.keys(tokens).map((t) => (
                      <Dropdown.Item
                        key={t}
                        content={
                          <DropDownContent
                            text={tokens[t].text}
                            desc={tokens[t].desc}
                            image={tokens[t].image}
                          />
                        }
                        text={tokens[t].text}
                        value={tokens[t].value}
                        onClick={(_, data) => {
                          setInput("to");
                          setTo({
                            ...to,
                            name: data.value,
                            symbol: data.text,
                            rates: 1,
                          });
                        }}
                      />
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              }
              labelPosition="right"
              placeholder="0.00"
              value={to.amount}
              onChange={(e) => {
                setInput("to");
                setTo({
                  ...to,
                  amount: e.target.value,
                  rates: 1,
                });
              }}
            />
            <BalanceLabel amount={to.balance} token={to.symbol} />
          </Form.Field>
          {loading
            ? null
            : from.rates &&
              to.rates && (
                <PriceLabel
                  amount_from={from.rates}
                  token_from={from.symbol}
                  amount_to={to.rates}
                  token_to={to.symbol}
                />
              )}

          <Button
            loading={loading}
            disabled={loading || !from.amount}
            circular
            compact
            style={{ margin: "8px 0" }}
            className="purple"
            content="Deposit"
            icon="right arrow"
            labelPosition="right"
          />
          <Button
            circular
            compact
            style={{ margin: "8px 16px 0" }}
            className="grey"
            content="Cancel"
            onClick={() => history.push("/pool")}
          />
        </Form>
      </Segment>
    </Page>
  );
};

const DropDownContent = ({ text, desc, image }) => (
  <div style={{ display: "flex", alignItems: "center" }}>
    <img
      src={image}
      width="20"
      height="20"
      alt={text}
      style={{ marginRight: 8 }}
    />
    <div className="column">
      <span style={{ fontWeight: "bold" }}>{text}</span>
      <span style={{ fontSize: ".7rem", color: "rgba(0,0,0,.5)" }}>{desc}</span>
    </div>
  </div>
);

DropDownContent.propTypes = {
  text: PropTypes.string,
  desc: PropTypes.string,
  image: PropTypes.string,
};

const BalanceLabel = ({ amount, token }) => (
  <div className="input-sublabel">
    <label>
      Balance: {amount} {token}
    </label>
  </div>
);

BalanceLabel.propTypes = {
  amount: PropTypes.number || PropTypes.string,
  token: PropTypes.string,
};

const PriceLabel = ({ amount_from, token_from, amount_to, token_to }) => (
  <div className="input-sublabel">
    <label>
      Price: {amount_from} {token_from} ≈ {amount_to} {token_to}
    </label>
  </div>
);

PriceLabel.propTypes = {
  amount_from: PropTypes.number || PropTypes.string,
  token_from: PropTypes.string,
  amount_to: PropTypes.number || PropTypes.string,
  token_to: PropTypes.string,
};

export default AddLiquidity;
