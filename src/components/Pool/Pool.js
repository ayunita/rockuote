import React, { useState, useEffect, useContext } from "react";
import { Dimmer, Loader, Segment, List, Image, Table, Button } from "semantic-ui-react";
import { useHistory } from "react-router-dom";
import Page from "../Layout/Page";
import tokens from "../../data/tokens.json";
import { Web3Context } from "../../context/Web3Context";
import { toNumber } from "../../helpers/decimal";
import { getAllPairsBalance } from "../../helpers/dex";

const Pool = () => {
  const history = useHistory();

  const web3State = useContext(Web3Context);
  const [loading, setLoading] = useState(false);
  const [pairs, setPairs] = useState([]);

  useEffect(() => {
    const load = async () => {
      if (!web3State.dex) return;
      setLoading(true);
      getAllPairsBalance(web3State.dex).then((result) => {
        let temp = Object.keys(result)
          .map((pair) => {
            let tokens = pair.split("/");
            return {
              source: tokens[0],
              target: tokens[1],
              amount1: result[pair].amount1,
              amount2: result[pair].amount2,
            };
          })
          .filter((p) => p.amount1 > 0 || p.amount2 > 0);
        setPairs([...temp]);
        setLoading(false);
      });
    };
    load();
  }, [web3State.dex]);

  return (
    <Page
      title="Current Liquidity"
      header={
        <Button
          compact
          circular
          className="purple"
          content="Add Liquidity"
          icon="plus"
          labelPosition="left"
          onClick={() => history.push("/add-liquidity")}
        />
      }
    >
      <Segment style={{ height: "100vh" }}>
        <Dimmer active={loading}>
          <Loader inverted content="Loading..." />
        </Dimmer>
        <LiquidityList data={pairs} />
      </Segment>
    </Page>
  );
};

const LiquidityList = ({ data }) => {
  return (
    <>
      <List relaxed animated verticalAlign="middle">
        {data.map((pair, index) => {
          return (
            <List.Item key={index}>
              <Image
                style={{ width: 20, height: 20 }}
                src={tokens[pair.source].image}
              />
              <Image
                style={{ width: 20, height: 20 }}
                src={tokens[pair.target].image}
              />
              <List.Content>
                <List.Header as="span">
                  {tokens[pair.source].text} / {tokens[pair.target].text}
                </List.Header>
                <List.Description className="liqudity-desc">
                  <Table className="liquidity-table">
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Total Pool Tokens</Table.HeaderCell>
                        <Table.HeaderCell>
                          Pooled {tokens[pair.source].text}
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                          Pooled {tokens[pair.target].text}
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>
                          {toNumber(Number(pair.amount1) + Number(pair.amount2))}
                        </Table.Cell>
                        <Table.Cell>{toNumber(pair.amount1)}</Table.Cell>
                        <Table.Cell>{toNumber(pair.amount2)}</Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </List.Description>
              </List.Content>
            </List.Item>
          );
        })}
      </List>
    </>
  );
};

export default Pool;
