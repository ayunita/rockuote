import React from "react";
import PropTypes from "prop-types";
import "./Landing.scss";

const Landing = ({ children }) => (
  <div className="full-screen relative bg-gradient">{children}</div>
);

Landing.propTypes = {
  children: PropTypes.any,
};

export default Landing;
