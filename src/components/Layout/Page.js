import React from "react";
import PropTypes from "prop-types";
import "react-notifications/lib/notifications.css";
import "./Page.scss";

const Page = ({ title, header, children }) => (
  <div className="page-form">
    <div className="form__header">
      <h1>{title}</h1>
      {header}
    </div>
    {children}
  </div>
);

Page.propTypes = {
  title: PropTypes.string.isRequired,
  header: PropTypes.any,
  children: PropTypes.any.isRequired,
};

export default Page;
