import React from "react";
import { Link, useRouteMatch } from "react-router-dom";
import WalletButton from "./WalletButton";
import "./Navigator.scss";

const Navigator = () => {
  const main = useRouteMatch("/");
  const swap = useRouteMatch("/swap");
  const pool = useRouteMatch("/pool");
  const addLiquidity = useRouteMatch("/add-liquidity");

  return (
    <div className="nav">
      <Link
        to="/swap"
        className={`nav__item ${main.isExact || swap ? "active" : ""}`}
      >
        Swap
      </Link>
      <Link to="/pool" className={`nav__item ${pool || addLiquidity ? "active" : ""}`}>
        Pool
      </Link>
      <WalletButton />
    </div>
  );
};

export default Navigator;
