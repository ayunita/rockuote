import React, { useEffect, useContext, useState } from "react";
import Web3 from "web3";
import { Button } from "semantic-ui-react";
import { NotificationManager } from "react-notifications";
import {
  getNetworkName,
  getWeb3
} from "../../helpers/wallet";
import { Web3ActionsContext } from "../../context/Web3Context";
import Dex from "../../abis/Dex.json";

const WalletButton = () => {
  const [web3Instance, setWeb3] = useState(null); 
  const [address, setAddress] = useState(null); // Account address
  const [network, setNetwork] = useState(null); // Network name
  const [dexInstance, setDex] = useState(null); // Network name
  const setWeb3State = useContext(Web3ActionsContext);

  // When page is refreshed, this will check whether wallet is already connected
  useEffect(() => {
    const checkConnection = async () => {
      // Check if browser is running Metamask
      let web3;
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        const accounts = await web3.eth.getAccounts();
        // Subscribe to changes in chain Id/network
        window.ethereum.on("chainChanged", function (chainId) {
          const id = parseInt(chainId);
          setNetwork(id);
          setWeb3State({ web3: web3, account: accounts[0], networkId: id, dex: dexInstance });
          // Show warning message if the network is not Kovan
          if (getNetworkName(id) !== "Kovan") {
            NotificationManager.warning(
              "Please switch to Kovan testnet!",
              "This network is not supported :(",
              1500
            );
          }
        });
        // Subscribe to changes in account
        window.ethereum.on("accountsChanged", function (accounts) {
          // If wallet disconnected, then reset address state and contract context
          if (accounts.length < 1) {
            setDex(null);
            setAddress(null);
            setNetwork(null);
            setWeb3State({ web3: null, account: null, networkId: null, dexInstance: null });
          } else {
            setAddress(accounts[0]);
            setWeb3State({
              web3: web3,
              account: accounts[0],
              networkId: network,
            });
          }
        });
      } else if (window.web3) {
        // TODO: handle other web3 browser
        web3 = new Web3(window.web3.currentProvider);
      }
      setWeb3Info(web3);
    };
    checkConnection();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Setup Dex Instance
  useEffect(() => {
    const createDexInstance = async () => {
      try {
        if (web3Instance) {
          const _dexInstance = new web3Instance.eth.Contract(
            Dex.abi, // Smart contract abi
            process.env.REACT_APP_CONTRACT_ADDRESS // Smart Contract address
          );

          if (_dexInstance) {
            setDex(_dexInstance);
            setWeb3State({ web3: web3Instance, account: address, networkId: network, dex: _dexInstance });
          }
        }
      } catch (e) {
        console.log("Dex Instance error:", e);
      }
    };
    createDexInstance();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [web3Instance]);

  // This function will connect to Provider wallet.
  // It will create and store web3 instance in the context, and update address state
  const connectWallet = async () => {
    const web3 = await getWeb3();
    if (web3) {
      setWeb3Info(web3);  
    }
  };

  // This function will set account address, network name, and contract context
  const setWeb3Info = async (web3) => {
    const accounts = await web3.eth.getAccounts();
    if (accounts.length > 0) {
      const address = accounts[0];
      // Set account address
      setAddress(address);
      const networkId = await web3.eth.net.getId();
      // Update network name
      setNetwork(networkId);
      // Show warning message if the network is not Kovan
      if (getNetworkName(networkId) !== "Kovan") {
        NotificationManager.warning(
          "Please switch to Kovan testnet!",
          "This network is not supported :(",
          1500
        );
      }
      setWeb3(web3);
      // Store web3 instance to the context
      setWeb3State({ web3: web3, account: address, networkId: networkId, dex: dexInstance });
    }
  };

  return (
    <Button
      compact
      circular
      className="green"
      style={{ marginLeft: 16 }}
      content={
        <div className="row vh-center">
          {address
            ? `${address.substring(0, 6)}...${address.substring(
                address.length - 4
              )}`
            : "Wallet"}
          <span style={{ marginLeft: 8, fontSize: ".6rem" }}>
            {getNetworkName(network)}
          </span>
        </div>
      }
      icon={address ? "heart" : "heart outline"}
      labelPosition="left"
      onClick={() => connectWallet()}
    />
  );
};

export default WalletButton;
