# Rockuote

[![Netlify Status](https://api.netlify.com/api/v1/badges/a75b953e-4cc7-47e6-8b6d-5b686163ff85/deploy-status)](https://app.netlify.com/sites/rockuote/deploys)

![picture](public/logo192.png)

Decentralized exchange that uses the request-for-quote model where a participant requests for quote on a specific asset. The price for the particular trade will be determined by the Chainlink oracle in which it reads from Ethereum Price Feeds.

**Demo: [https://rockuote.netlify.app/](https://rockuote.netlify.app/)**

## Getting Started

In order to run the project in localhost or testnet, please follow these steps.

### Prerequisites

1.  npm/node: [https://nodejs.org/en/](https://nodejs.org/en/)
2.  Infura: [https://infura.io/](https://infura.io/)
3.  some assets (on Kovan testnet):
    *  ETH: [https://faucet.kovan.network/](https://faucet.kovan.network/)
	*  LINK: [https://kovan.chain.link/](https://kovan.chain.link/)
	*  UNI: [https://app.uniswap.org/#/swap](https://app.uniswap.org/#/swap)

### Installing

On root directory, create `.env` file
```
REACT_APP_INFURA_ID=<Infura ID>
REACT_APP_CONTRACT_ADDRESS=<Dex Contract Address on Kovan>
```

Install all dependencies
```
npm install
```

Run the client
```
npm run start
```

## Running the tests

Test in progress ...

## Deployment

Deploy App
```
npm run build
```

Please use [Remix](https://remix.ethereum.org/) to deploy Smart Contract to Kovan testnet

## Built With

* [Create React App](https://reactjs.org/docs/create-a-new-react-app.html) - The web framework used
* [Semantic UI](https://semantic-ui.com/) - UI library
* [Ethereum](https://ethereum.org/) - Blockchain used
* [web3.js](https://web3js.readthedocs.io/) - Ethereum Javascript API
* [Remix](https://remix.ethereum.org/) - Ethereum IDE
* [Infura](https://infura.io/) - Tools to connect to Ethereum blockchain

## Authors

* **Andriani Yunita** - [Bitbucket](https://bitbucket.org/ayunita)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details